#' Title
#' Get Fit Compounds
#' @param tcta_list
#' @param nodeID
#' @param timeselect
#' @param concselect
#' @param concentrations
#' @param output
#' @param logx
#'
#' @return
#' @export
#'
#' @examples
get_fit_compounds <- function(tcta_list, nodeID, timeselect, concselect, concentrations, output = "plot",logx = F) {
    if (!is.na(tcta_list[[1]][nodeID,1])) {
    nodeplotlist_all <- lapply(X = names(tcta_list), FUN = function(substance) {


            if(logx){
                timeconc <-
                    expand.grid(
                        time_hpe = timeselect,
                        concentration_umol_l = emdbook::lseq(
                            min(concselect[[substance]]),
                            max(concselect[[substance]]),
                            length.out = 15
                        )
                    )
            }else{
                timeconc <-
                    expand.grid(
                        time_hpe = timeselect,
                        concentration_umol_l = seq(
                            min(concselect[[substance]]),
                            max(concselect[[substance]]),
                            length.out = 15
                        )
                    )
            }




            # aggregate fitted data
            D_fit <- data.frame(
                logFC_hill = NA,
                upr_hill = NA,
                lwr_hill = NA,
                logFC_gauss = NA,
                upr_gauss = NA,
                lwr_gauss = NA,
                time_hpe = timeconc$time_hpe,
                concentration_umol_l = timeconc$concentration_umol_l,
                substance = substance
            )

            # fit for hill-gauss model ----------------------------------------------------

            D_fit$logFC_hill <- toxprofileR::hill_gauss(
                dose = D_fit$concentration_umol_l,
                time = D_fit$time_hpe,
                hillslope = as.numeric(tcta_list[[substance]][nodeID, "hillslope_best_hill"]),
                maxS50 = as.numeric(tcta_list[[substance]][nodeID, "maxS50_best_hill"]),
                mu = as.numeric(tcta_list[[substance]][nodeID, "mu_best_hill"]),
                sigma = as.numeric(tcta_list[[substance]][nodeID, "sigma_best_hill"]),
                maxGene = as.numeric(tcta_list[[substance]][nodeID, "max_best_hill"])
            )

            D_fit$lwr_hill <-
                D_fit$logFC_hill - (qnorm(0.95) * as.numeric(tcta_list[[substance]][nodeID, "err_best_hill"]))
            D_fit$upr_hill <-
                D_fit$logFC_hill + (qnorm(0.95) * as.numeric(tcta_list[[substance]][nodeID, "err_best_hill"]))

            # fit for gauss-gauss model ----------------------------------------------------

            D_fit$logFC_gauss <- toxprofileR::gauss_gauss(
                dose = D_fit$concentration_umol_l,
                time = D_fit$time_hpe,
                mconc = as.numeric(tcta_list[[substance]][nodeID, "mconc_best_gauss"]),
                sconc = as.numeric(tcta_list[[substance]][nodeID, "sconc_best_gauss"]),
                mu = as.numeric(tcta_list[[substance]][nodeID, "mu_best_gauss"]),
                sigma = as.numeric(tcta_list[[substance]][nodeID, "sigma_best_gauss"]),
                maxGene = as.numeric(tcta_list[[substance]][nodeID, "max_best_gauss"])
            )

            D_fit$lwr_gauss <-
                D_fit$logFC_gauss - (qnorm(0.95) * as.numeric(tcta_list[[substance]][nodeID, "err_best_gauss"]))
            D_fit$upr_gauss <-
                D_fit$logFC_gauss + (qnorm(0.95) * as.numeric(tcta_list[[substance]][nodeID, "err_best_gauss"]))


            D_fit <- D_fit[D_fit$time_hpe %in% timeselect,]


            D_fit$time_name <-
                factor(
                    paste(D_fit$time_hpe, "hpe"),
                    levels = paste(sort(unique(D_fit$time_hpe)), "hpe")
                )

            D_fit$nodeID <- nodeID

            D_fit$maxGene <- as.numeric(tcta_list[[substance]][nodeID, "max_best_hill"])

            return(D_fit)


    })

    D_fit_all <-
        do.call("rbind",nodeplotlist_all)
    D_fit_all$substance <- as.character(D_fit_all$substance)
    D_fit_all <- as.data.table(D_fit_all[!is.na(D_fit_all$substance), ])

    D_fit_all[, log_unaffected_fraction := log(1-(logFC_hill/maxGene))][]
    D_fit_all$mixture_fraction <- as.numeric(concentrations[D_fit_all$substance])
    D_fit_all$mixture_concentration <- round(D_fit_all$concentration_umol_l/D_fit_all$mixture_fraction, digits = 2)
    D_fit_all[, sum_unaffected_fraction:=sum(log_unaffected_fraction), by = .(mixture_concentration, time_hpe)]
    D_fit_all[, sum_logFC:=sum(logFC_hill), by = .(mixture_concentration, time_hpe)]

   return(D_fit_all)
    } else {
        return(NA)
    }
}



#' Calculate Toxic Units IA
#'
#' @param compound_fit
#'
#' @return
#' @export
#'
#' @examples
calc_tus_IA <- function (compound_fit)
{
    if (is.data.frame(compound_fit)) {
        aucs_components <- do.call("rbind", lapply(unique(compound_fit$substance),
                                        function(sub) {
                                            auc_time <- lapply(unique(compound_fit$time_hpe[compound_fit$substance==sub]), function(time){
                                            MESS::auc(x = compound_fit$mixture_concentration[compound_fit$substance ==
                                                                                             sub&compound_fit$time_hpe==time], y = compound_fit$log_unaffected_fraction[compound_fit$substance ==
                                                                                                                      sub&compound_fit$time_hpe==time])
                                            })
                                            data.frame(auc = sum(unlist(auc_time)), substance = sub, stringsAsFactors = F)
                                                }))

        aucs_fractions <- aucs_components$auc/sum(aucs_components$auc,
                                              na.rm = T)
        names(aucs_fractions) <- aucs_components$substance
        aucs_fractions
    }
    else {
        aucs_fractions <- c(NA, NA, NA)
        aucs_fractions
    }
}


#' Calculate Toxic Units EA
#'
#' @param compound_fit
#'
#' @return
#' @export
#'
#' @examples
calc_tus_EA <- function (compound_fit)
{
    if (is.data.frame(compound_fit)) {
        aucs_components <- do.call("rbind", lapply(unique(compound_fit$substance),
                                                   function(sub) {
                                                       auc_time <- lapply(unique(compound_fit$time_hpe[compound_fit$substance==sub]), function(time){
                                                           MESS::auc(x = compound_fit$mixture_concentration[compound_fit$substance ==
                                                                                                                sub&compound_fit$time_hpe==time], y = abs(compound_fit$logFC_hill[compound_fit$substance ==
                                                                                                                                                                                               sub&compound_fit$time_hpe==time]))
                                                       })
                                                       data.frame(auc = sum(unlist(auc_time)), substance = sub, stringsAsFactors = F)
                                                   }))

        aucs_fractions <- aucs_components$auc/sum(aucs_components$auc,
                                                  na.rm = T)
        names(aucs_fractions) <- aucs_components$substance
        aucs_fractions
    }
    else {
        aucs_fractions <- c(NA, NA, NA)
        aucs_fractions
    }
}
