#' Calculate prediction deviation ratio
#'
#' @param mixpred
#' @param tcta_list_mix
#' @param parametername
#' @param model
#' @param nodeIDs
#'
#' @return
#' @export
compare_predicted_parameters <-
    function(mixpred,
             tcta_list_mix,
             parametername = "maxS50_best_hill",
             model,
             nodeIDs) {
        unlist(lapply(nodeIDs, function(node) {
            if (model == "CA") {
                param_fit <-
                    mixpred$fit_CA[mixpred$fit_CA$node == node, parametername]
                param_measured <-
                    as.numeric(tcta_list_mix[node, parametername])
                pdr <- param_fit / param_measured
            }

            if (model == "IA") {
                param_fit <-
                    mixpred$fit_IA[mixpred$fit_IA$node == node, parametername]
                param_measured <-
                    as.numeric(tcta_list_mix[node, parametername])
                pdr <- param_fit / param_measured
            }

            if (model == "EA") {
                param_fit <-
                    mixpred$fit_EA[mixpred$fit_EA$node == node, parametername]
                param_measured <-
                    as.numeric(tcta_list_mix[node, parametername])
                pdr <- param_fit / param_measured
            }

            pdr
        }))
    }
