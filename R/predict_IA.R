

#' Predict IA (mobi)
#'
#' @param params_IA_hill_node
#' @param fractions
#' @param doseseq
#' @param timepoints
#' @param AIC_threshold
#' @param effect_ratio_threshold
#'
#' @return
#' @export
#'
predict_IA_hill <-
    function(params_IA_hill_node,
             fractions,
             doseseq,
             timepoints,
             AIC_threshold = 0.9,
             effect_ratio_threshold = 0.2
             ) {
        FCmax <-
            max(c(unlist(
                lapply(params_IA_hill_node, function(fitparams) {
                    fitparams$maxGene
                })
            ), 0), na.rm = T)
        FCmin <-
            min(c(unlist(
                lapply(params_IA_hill_node, function(fitparams) {
                    fitparams$maxGene
                })
            ), 0), na.rm = T)
        FCextremes <- c(FCmin, FCmax)

        IA_sampled <-
            lapply(1:nrow(params_IA_hill_node[[1]]), function(x) {
                EC_complete <-
                    lapply(timepoints, function(timep) {
                        EC_mix <- lapply(params_IA_hill_node, function(fitparams) {
                            ECfracs <- data.frame(
                                logFC = omixR::mobi.err(
                                    dose = doseseq * as.numeric(fitparams$fraction)[x],
                                    time = timep,
                                    hillslope = as.numeric(fitparams$hillslope)[x],
                                    maxS50 = as.numeric(fitparams$maxS50)[x],
                                    mu = as.numeric(fitparams$mu)[x],
                                    sigma = as.numeric(fitparams$sigma)[x],
                                    maxGene = as.numeric(fitparams$maxGene)[x],
                                    err = as.numeric(fitparams$err_sample)[x]
                                ),
                                time = timep,
                                mixtureconc = doseseq,
                                substance = fitparams$substance[x],
                                samplenr = x
                            )

                            # set response to 0 if model does not describe significant change
                            effect_ratio <- fitparams$maxGene_sub[x]/fitparams$maxGene[x]

                            if (effect_ratio < effect_ratio_threshold |
                                fitparams$AICw[x] < AIC_threshold) {
                                ECfracs <-
                                    data.frame(
                                        logFC = as.numeric(fitparams$err_sample)[x],
                                        time = timep,
                                        mixtureconc = doseseq,
                                        substance = fitparams$substance[x],
                                        samplenr = x
                                    )
                            }

                            ECfracs
                        })

                        do.call('rbind', EC_mix)
                    })

                ECfracs_all_IA <- do.call('rbind', EC_complete)

                IA <-
                    apply(expand.grid(doseseq, timepoints), MARGIN = 1, function(td) {
                        fracs <-
                            ECfracs_all_IA[ECfracs_all_IA$mixtureconc == as.numeric(td[1]) &
                                               ECfracs_all_IA$time == as.numeric(td[2]), ]
                        fracs_up <-
                            fracs[fracs$logFC > 0 & !is.na(fracs$logFC), ]
                        fracs_down <-
                            fracs[fracs$logFC < 0 & !is.na(fracs$logFC), ]
                        fracs_up$fraction <-
                            fracs_up$logFC / max(FCextremes, na.rm = T)
                        IA_up <-
                            (1 - prod(1 - fracs_up$fraction)) * FCextremes[FCextremes > 0]
                        fracs_down$fraction <-
                            fracs_down$logFC / min(FCextremes, na.rm = T)
                        IA_down <-
                            (1 - prod(1 - fracs_down$fraction)) * FCextremes[FCextremes < 0]
                        IAtotal <- sum(c(IA_up, IA_down))
                        return(IAtotal)
                    })

                IAframe <-
                    data.frame(
                        logFC = IA,
                        time = expand.grid(doseseq, timepoints)[, 2],
                        mixtureconc = expand.grid(doseseq, timepoints)[, 1],
                        substance = "IA",
                        samplenr = x
                    )

                return(list(IA = IAframe, EC_fracs_all_IA = ECfracs_all_IA))
            })

        IA <- do.call("rbind", lapply(IA_sampled, function(x){x$IA}))
        EC_fracs_all_IA <- do.call("rbind", lapply(IA_sampled, function(x){x$EC_fracs_all_IA}))

        IA$model <- "IA"
        EC_fracs_all_IA$model <- "IA"


        return(list(IA = IA, EC_fracs_all_IA = EC_fracs_all_IA))
    }
